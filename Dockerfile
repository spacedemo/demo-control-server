FROM openjdk:8-jre-slim

COPY dcs-app/target/dcs-app.jar .

CMD ["java", "-jar", "dcs-app.jar"]
