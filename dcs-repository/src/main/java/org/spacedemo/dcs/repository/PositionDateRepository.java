package org.spacedemo.dcs.repository;

import java.time.Instant;

import org.spacedemo.dcs.model.PositionDate;
import org.springframework.data.repository.CrudRepository;

public interface PositionDateRepository extends CrudRepository<PositionDate, String> {

    public PositionDate findByDate(Instant Date);

}
