package org.spacedemo.dcs.controller;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.spacedemo.dcs.model.PositionDate;
import org.spacedemo.dcs.model.PositionDates;
import org.spacedemo.dcs.repository.PositionDateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
public class PositionController {

	@Autowired
	private PositionDateRepository positionDateRepository;

	@Value("${fetch.position.step_ms}")
	private Integer fetchPositionStepDelay;

	@Value("${fetch.position.number}")
	private Integer fetchPositionNumber;

	@Scheduled(fixedDelayString = "${fetch.position.delay_ms}")
	public void fetchSatellitePosition() {
		RestTemplate restTemplate = new RestTemplate();
		 
	    String uri = "http://localhost:8080/position";
	    
	    UriComponents builder = UriComponentsBuilder.fromHttpUrl(uri)
	    		.queryParam("begin",Instant.parse("2020-01-26T00:00:00.000Z"))
	            .queryParam("end",Instant.parse("2020-01-26T00:01:00.000Z"))
	            .queryParam("step_ms",1000).build();

		PositionDates ephem = restTemplate.getForObject(builder.toUriString(), PositionDates.class);
		
		
		System.out.println(ephem);
 
	}

	@Value("${sync.position.number}")
	private String syncPositionNumber;

	@Scheduled(fixedDelayString = "${sync.position.delay_ms}")
	public void syncSatellitePosition() {

	}

}
